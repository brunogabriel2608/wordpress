<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@400;500;600;700&display=swap" rel="stylesheet"/>

   
    <title> <?php bloginfo('lobinhos') ?></title>
    <?php wp_head(); ?>
  </head>
  <body>
    <header>
      <div class="header">
        <a href="http://tarefainjunior.local/lista-de-lobos/">Nossos Lobinhos

        </a>
        <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/logo.png" alt="" class="logo" />
        <a href="http://tarefainjunior.local/pagina-quemsomos/">Quem Somos</a>
      </div>
    </header>