<?php
// Template Name: paginaLista
?>

<?php 
  get_header(); 
  nomeescolhido();
  my_theme_scripts_function();

?>

    <section class="list">
        <div class="pesquisar">
            <input type="text" class="text" id="search" onkeyup="search()">
            <a href="../Adicionar Lobo Page/addlobo.html"><input type="button" value="+ Lobo" class="Buttom" ></a>
        </div>
        <div class="divfilter">
            <input type="checkbox" name="" id="filter" value="false">
            <span>Ver lobinhos adotados</span>
        </div> 
        <div class="container">
        <?php
          $numero = 0;
        ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php if($numero % 2 == 0){ ?>
        <section class="wolf">
            <div class="moldura">

              <div class="frame"></div>

              <?php if( get_field('lobo_imagem') ): ?>
                  <img src="<?php the_field('lobo_imagem'); ?>" alt="" srcset="" width="380" height="320" id="Image">
              <?php endif; ?>


            </div>
            <div class="text">
              <div class="divnome-lobo" ><h1 class='nome-lobo'> <?php the_field('lobo_nome'); ?> </h1><a href="../showLobinho/showLobinho.html"><input type='button' value='Adotar' id='adopted${i}' class='adopted'></a></div>
              <div class="idade-lobo" id="idade">idade: <?php the_field('lobo_idade'); ?> anos</div>
              <div class="descricao" id="description"> <?php the_field('lobo_descricao'); ?> </div>
            </div>
          </section>
          <?php }
          else{ ?>
          
          
          <section class="wolfinverted" id="inverted">
            
           
            <div class="invertedMoldura">

              <div class="frame"></div>
              <?php if( get_field('lobo_imagem') ): ?>
                  <img src="<?php the_field('lobo_imagem'); ?>" width="380" height="320" class="invertedImage">
              <?php endif; ?>    
            </div>

            <div class="text-inverted">
              <div class="divnome-invertido"><h1 class="nome-inverted"> <?php the_field('lobo_nome'); ?> </h1><a href="../showLobinho/showLobinho.html"><input type='button' value='Adotar' id='adopted${i}' class='adopted'></a></div>
              <div class="idade-invertido">idade: <?php the_field('lobo_idade'); ?> anos</div>
              <div class="descricao-invertido"> <?php the_field('lobo_descricao'); ?> </div>
            </div>
          </section>
        <?php }
        $numero++; ?>
            
            <?php endwhile; else: ?>
            <p>desculpe, o post não segue os critérios 
            escolhidos</p>
            <?php endif; ?>
            <div class='pagination'><?php my_pagination();?></div>
        </div>
    </section>
    <div class="divider"></div>
    <?php get_footer(); ?>
