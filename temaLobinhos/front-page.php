<?php
// Template Name: pagina inicial
?>
    
    
    <?php 
      get_header(); 
      style();
    ?>

    <main>
      <section class="loboInicial">
        <div class="textos">
          <h1><?php the_field('titulo_inicial'); ?></h1>
          <pre class="underline"> ___</pre>
          <p><?php the_field('descricao_inicial'); ?></p>
        </div>
      </section>
      <section class="about">
        <div class="divAbout">
          <h2><?php the_field('titulo_sobre'); ?></h2>
          <p><?php the_field('descricao_sobre'); ?></p>
        </div>
      </section>
      <section class="valores">
        <h2><?php the_field('valores_titulo'); ?></h2>
        <div class="cards">
          <div class="card">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/icone1.png" alt="" srcset="" />
            <h3><?php the_field('valores_subtitulo_1'); ?></h3>
            <p class="pCard">
              <?php the_field('valores_texto_1'); ?>
            </p>
          </div>
          <div class="card">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/icone2.png" alt="" srcset="" />
            <h3><?php the_field('valores_subtitulo_2'); ?></h3>
            <p class="pCard">
              <?php the_field('valores_texto_2'); ?>
            </p>
          </div>
          <div class="card">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/icone3.png" alt="" srcset="" />
            <h3><?php the_field('valores_subtitulo_3'); ?></h3>
            <p class="pCard">
            <?php the_field('valores_texto_3'); ?>
            </p>
          </div>
          <div class="card">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/icone4.png" alt="" srcset="" />
            <h3><?php the_field('valores_subtitulo_4'); ?></h3>
            <p class="pCard">
            <?php the_field('valores_texto_4'); ?>
            </p>
          </div>
        </div>
      </section>
      <section class="loboExemple">
        <div class="titulo-exemplo">
          <h2>Lobos Exemplo</h2>
        </div>
        <?php
          $querry = new WP_Query( 'posts_per_page=2' );
        ?>
      <?php
          $numero = 0;
        ?>
        <?php
          while($querry -> have_posts()) : $querry -> the_post();
        ?>

        <?php if($numero % 2 == 0){ ?>

        <div class="container">
          <div class="wolf">
            <div class="moldura">

              <div class="frame"></div>

              <?php if( get_field('lobo_imagem') ): ?>
                  <img src="<?php the_field('lobo_imagem'); ?>" width="300" height="300">
              <?php endif; ?>


            </div>
            <div class="text">
              <div class="nome-lobo" ><h1 id="nome"> <?php the_field('lobo_nome'); ?> </h1></div>
              <div class="idade-lobo" id="idade">idade: <?php the_field('lobo_idade'); ?> anos</div>
              <div class="descricao" id="description"> <?php the_field('lobo_descricao'); ?> </div>
            </div>
          </div>
          <?php }
          else{ ?>
          
          
          <div class="wolf" id="inverted">
            <div class="moldura" id="invertedMoldura">

              <div class="frame"></div>
              <?php if( get_field('lobo_imagem') ): ?>
                  <img src="<?php the_field('lobo_imagem'); ?>" width="300" height="300" id="invertedImage">
              <?php endif; ?>    
            </div>
            <div class="text" id="text-inverted">
              <div class="nome-lobo"><h1 id="nome-invertido"> <?php the_field('lobo_nome'); ?> </h1></div>
              <div class="idade-lobo" id="idade-invertido">idade: <?php the_field('lobo_idade'); ?> anos</div>
              <div class="descricao" id="descricao-invertido"> <?php the_field('lobo_descricao'); ?> </div>
            </div>
        </div>
        <?php }
        $numero++; ?>
        
        <?php 
          endwhile;
          wp_reset_postdata(); 
        ?>
        
      </section>
    </main>
    <div class="divider"></div>
    
    <?php get_footer(); ?>