<?php
// Template Name: paginaQuemsomos
?>
<?php 
  get_header();
  quemsomos();

?>
    <section class="about">
      <div class="divAbout">
        <div class="titulo">
          <h2><?php the_title(); ?></h2>
        </div>
        <div class="text">
          <p>
          <?php the_content(); ?>


          </p>
        </div>
        
      </div>
    </section>
    <div class="divider"></div>
    <?php get_footer(); ?>
