

function search(){
    let input = document.querySelector('#search')
    let filter = input.value.toUpperCase()
    let menu = document.querySelector('.container')
    let itensMenu = menu.getElementsByTagName('section')
    
    for(i=0; i<itensMenu.length; i++){
        names = itensMenu[i].getElementsByTagName('h1')[0]
        if(names.innerHTML.toUpperCase().indexOf(filter)>-1){
            itensMenu[i].style.display = ''
        }else{
            itensMenu[i].style.display = 'none'
        }
    }
}

